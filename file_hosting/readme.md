
Project Setup
----------------------
docker-compose build

docker-compose up

Celery Task 
-----------------
celery -A file_hosting worker -l info

celery -A file_hosting beat -l info -S django

Celery task is running every day midnight to clear cache