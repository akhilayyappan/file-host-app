from django.contrib.auth import get_user_model

from rest_framework.generics import (
    CreateAPIView,
)
from rest_framework.permissions import (
    AllowAny,

)

User = get_user_model()

from .serializers import (
    UserCreateSerializer,
)

'''
User register APIView
'''


class UserCreateAPIView(CreateAPIView):
    serializer_class = UserCreateSerializer
    queryset = User.objects.all()
    permission_classes = [AllowAny]


