from rest_framework.generics import (
    DestroyAPIView,
    ListAPIView,
    RetrieveAPIView,
)

from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,

)

from .serializers import EventListSerializer, EventDetailSerializer

from events.models import Event


class EventListView(ListAPIView):
    serializer_class = EventListSerializer
    permission_classes = [AllowAny]
    queryset = Event.objects.all()


class EventDetailView(RetrieveAPIView):
    queryset = Event.objects.all()
    serializer_class = EventDetailSerializer
    lookup_field = 'id'
    permission_classes = [AllowAny]
