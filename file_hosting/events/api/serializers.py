from events.models import Event

from rest_framework.serializers import (
    HyperlinkedIdentityField,
    ModelSerializer,
    SerializerMethodField
)
from account.api.serializers import UserDetailSerializer


class EventListSerializer(ModelSerializer):
    user = UserDetailSerializer(read_only=True)

    class Meta:
        model = Event
        fields = [
            'id',
            'user',
            'host',
            'path',
            'uri',
            'status_code',
        ]


class EventDetailSerializer(ModelSerializer):
    user = UserDetailSerializer(read_only=True)

    class Meta:
        model = Event
        fields = [
            'id',
            'user',
            'host',
            'path',
            'uri',
            'status_code',
            'time',
            'user_agent',
            'remote_addr',
            'remote_addr_fwd',
            'meta',
            'cookies',
            'is_secure',
            'is_ajax',
        ]
