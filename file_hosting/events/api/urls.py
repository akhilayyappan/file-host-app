from django.urls import path, re_path

from .views import (
    EventListView,
    EventDetailView,
)

urlpatterns = [
    path('', EventListView.as_view(), name='list'),
    re_path(r'(?P<id>[0-9]+)/$', EventDetailView.as_view(), name='detail'),
]