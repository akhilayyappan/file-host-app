import json
import sys

from events.models import Event
from django.contrib.auth import get_user_model
from django.utils.deprecation import MiddlewareMixin
from rest_framework_jwt.serializers import VerifyJSONWebTokenSerializer

User = get_user_model()

'''
Middleware to store all the API requests 
'''


def dumps(value):
    return json.dumps(value, default=lambda o: None)


class WebRequestMiddleware(MiddlewareMixin):
    def process_view(self, request, view_func, view_args, view_kwargs):
        setattr(request, 'hide_post', view_kwargs.pop('hide_post', False))

    def process_response(self, request, response):
        if request.path.endswith('/favicon.ico'):
            return response
        if request.path.startswith('/api') or request.path.startswith('/file-access'):

            try:
                self.save(request, response)
            except Exception as e:
                print(sys.stderr, "Error saving request log", e)

        return response

    def save(self, request, response):

        if hasattr(request, 'user'):
            user = request.user
        else:
            user = None

        if not isinstance(user, User):
            token = request.META.get('HTTP_AUTHORIZATION', " ").split(' ')[1]
            data = {'token': token}
            try:
                valid_data = VerifyJSONWebTokenSerializer().validate(data)
                user = valid_data['user']
            except Exception as e:
                print("validation error", e)

        meta = request.META.copy()
        meta.pop('QUERY_STRING', None)
        meta.pop('HTTP_COOKIE', None)
        remote_addr_fwd = None

        if 'HTTP_X_FORWARDED_FOR' in meta:
            remote_addr_fwd = meta['HTTP_X_FORWARDED_FOR'].split(",")[0].strip()
            if remote_addr_fwd == meta['HTTP_X_FORWARDED_FOR']:
                meta.pop('HTTP_X_FORWARDED_FOR')

        post = None
        uri = request.build_absolute_uri()
        if request.POST and uri != '/login/':
            post = dumps(request.POST)
        Event.objects.create(
            host=request.get_host(),
            path=request.path,
            method=request.method,
            uri=request.build_absolute_uri(),
            status_code=response.status_code,
            user_agent=meta.pop('HTTP_USER_AGENT', None),
            remote_addr=meta.pop('REMOTE_ADDR', None),
            remote_addr_fwd=remote_addr_fwd,
            meta=None if not meta else dumps(meta),
            cookies=None if not request.COOKIES else dumps(request.COOKIES),
            is_secure=request.is_secure(),
            is_ajax=request.is_ajax(),
            user=user,
        )
