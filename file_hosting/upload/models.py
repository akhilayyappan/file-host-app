from django.db import models

from django.contrib.auth import get_user_model

# Create your models here.

User = get_user_model()

'''
method to create user specific folder while uploading the file to s3 bucket
'''


def upload_to(instance, filename):
    return '%s-%s/%s' % (instance.user.username, instance.user.id, filename)


class FileUpload(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    file = models.FileField(upload_to=upload_to)
    file_name = models.CharField(max_length=600, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.username


'''
Model to track the number of times the file being accessed
'''


class AccessCount(models.Model):
    access_file = models.ForeignKey(FileUpload, on_delete=models.CASCADE)
    count = models.IntegerField(default=1)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.access_file.file_name
