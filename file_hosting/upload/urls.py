from django.urls import path, re_path

from .views import FileAccessDetailView

urlpatterns = [
    path('<int:pk>/', FileAccessDetailView.as_view(), name='detail'),
]
