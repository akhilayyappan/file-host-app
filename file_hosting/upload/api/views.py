from rest_framework.exceptions import APIException

from rest_framework.generics import (
    CreateAPIView,
    DestroyAPIView,
    ListAPIView,
    UpdateAPIView,
    RetrieveAPIView,
)

from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
)

from .permissions import IsOwnerOrReadOnly

from .serializers import FileUploadCreateUpdateSerializer, FileUploadDetailSerializer, FileUploadListSerializer

from upload.models import FileUpload


'''
File upload create APIView
'''

class FileUploadCreateView(CreateAPIView):
    queryset = FileUpload.objects.all()
    serializer_class = FileUploadCreateUpdateSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        file_name = self.request.data.get('file')
        try:
            serializer.save(user=self.request.user, file_name=file_name)
        except Exception as e:
            raise APIException("File already exists")

'''
File upload list APIView
'''
class FileUploadListView(ListAPIView):
    serializer_class = FileUploadListSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        queryset = FileUpload.objects.filter(user=user)
        return queryset

'''
File upload update APIView
'''
class FileUploadUpdateView(UpdateAPIView):
    queryset = FileUpload.objects.all()
    serializer_class = FileUploadCreateUpdateSerializer
    lookup_field = 'id'
    permission_classes = [IsOwnerOrReadOnly]

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)


'''
File upload delete APIView
'''
class FileUploadDeleteAPIView(DestroyAPIView):
    queryset = FileUpload.objects.all()
    serializer_class = FileUploadDetailSerializer
    lookup_field = 'id'
    permission_classes = [IsOwnerOrReadOnly]
