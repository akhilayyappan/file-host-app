from rest_framework.serializers import (
    HyperlinkedIdentityField,
    ModelSerializer,
    SerializerMethodField
)

from upload.models import FileUpload
from account.api.serializers import UserDetailSerializer

'''
File upload create update serializer
'''


class FileUploadCreateUpdateSerializer(ModelSerializer):
    class Meta:
        model = FileUpload
        fields = [
            'file'
        ]


fileupload_detail_url = HyperlinkedIdentityField(
    view_name='uploads:detail',
    lookup_field='pk'
)

'''
File upload detail serializer
'''


class FileUploadDetailSerializer(ModelSerializer):
    url = fileupload_detail_url
    user = UserDetailSerializer(read_only=True)
    file = SerializerMethodField()

    class Meta:
        model = FileUpload
        fields = [
            'url',
            'id',
            'user',
            'file',
            'created_at',
            'updated_at',
        ]

    def get_file(self, obj):
        try:
            file = obj.file.url
        except:
            file = None
        return file


'''
File upload list serializer
'''
class FileUploadListSerializer(ModelSerializer):
    file_url = fileupload_detail_url
    user = UserDetailSerializer(read_only=True)

    class Meta:
        model = FileUpload
        fields = [
            'file_url',
            'id',
            'user',
        ]
