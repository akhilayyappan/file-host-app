from django.urls import path, re_path

from .views import (
    FileUploadListView,
    FileUploadCreateView,
    FileUploadUpdateView,
    FileUploadDeleteAPIView
    )

urlpatterns = [
    path('', FileUploadListView.as_view(), name='list'),
    path('create/', FileUploadCreateView.as_view(), name='create'),
    re_path(r'(?P<id>[0-9]+)/edit/$', FileUploadUpdateView.as_view(), name='update'),
    re_path(r'(?P<id>[0-9]+)/delete/$', FileUploadDeleteAPIView.as_view(), name='delete')
]