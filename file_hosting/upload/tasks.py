from __future__ import absolute_import, unicode_literals
from celery.decorators import task
from datetime import datetime, timedelta
from django.core.cache import cache

from .models import AccessCount


@task(name="clear_cache")
def clear_cache():
    date_threshold = datetime.now() - timedelta(days=7)
    access_files = AccessCount.objects.filter(count__gte=5, created_at__lte=date_threshold)
    file_ids = [file_obj.access_file.id for file_obj in access_files]
    cache.delete_many(file_ids)
    access_files.delete()
