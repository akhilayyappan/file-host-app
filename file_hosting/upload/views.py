from django.shortcuts import render

from django.core.cache import cache

from django.views.generic import DetailView
from django.http import HttpResponseRedirect

from .models import FileUpload, AccessCount

# Create your views here.
'''
method to get the file from cache. If the file is not found in the cache it will store in it.
'''


def cache_file(file_obj):
    cache_key = file_obj.id
    cache_time = 604800
    data = cache.get(cache_key)
    if not data:
        data = file_obj.file.url
        cache.set(cache_key, data, cache_time)
    return data


class FileAccessDetailView(DetailView):
    model = FileUpload

    def get(self, request, *args, **kwargs):
        file_path = None
        file_obj = FileUpload.objects.get(id=self.kwargs['pk'])
        try:
            obj = AccessCount.objects.get(access_file=file_obj)
            if obj.count >= 5:  # file are caching the file count more than 5
                file_path = cache_file(file_obj)
                obj.count = obj.count + 1
            else:
                obj.count = obj.count + 1
            obj.save()
        except Exception as e:
            AccessCount.objects.create(access_file=file_obj)
        if not file_path:
            file_path = file_obj.file.url
        return HttpResponseRedirect(file_path)
